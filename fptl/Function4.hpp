/*
 * Function Pointer Template Library V1.0
 * Copyright (c) 2013-2014 Christopher D. Granz
 *
 * www.christophergranz.com
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgement in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 * distribution.
 */

#pragma once

#include "FunctionTypes.hpp"

namespace fptl {

// we define these here to make it easier to define new versions
// of this class template which takes a variable number of template parameters
#define FPTL_TEMPLATE_NAME				Function4
#define FPTL_TEMPLATE_TYPENAME_ARGS		typename R, typename P0, typename P1, typename P2, typename P3
#define FPTL_TEMPLATE_ARGS				R, P0, P1, P2, P3
#define FPTL_PARAM_TYPE_LIST			P0 p0, P1 p1, P2 p2, P3 p3
#define FPTL_PARAM_LIST					p0, p1, p2, p3

#include "FunctionInternal.hpp"

#undef FPTL_TEMPLATE_NAME
#undef FPTL_TEMPLATE_TYPENAME_ARGS
#undef FPTL_TEMPLATE_ARGS
#undef FPTL_TEMPLATE
#undef FPTL_PARAM_TYPE_LIST
#undef FPTL_PARAM_LIST

} // namespace fptl

