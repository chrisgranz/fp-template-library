/*
 * Function Pointer Template Library V1.0
 * Copyright (c) 2013-2014 Christopher D. Granz
 *
 * www.christophergranz.com
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgement in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 * distribution.
 */

#pragma once

#if defined(FPTL_TEMPLATE_NAME) \
	|| defined(FPTL_TEMPLATE_TYPENAME_ARGS) \
	|| defined(FPTL_TEMPLATE_ARGS) \
	|| defined(FPTL_TEMPLATE) \
	|| defined(FPTL_PARAM_TYPE_LIST) \
	|| defined(FPTL_PARAM_LIST)
#error "FPTL: Do not define any of these constants: FPTL_TEMPLATE_NAME, FPTL_TEMPLATE_TYPENAME_ARGS, FPTL_TEMPLATE_ARGS, FPTL_TEMPLATE, FPTL_PARAM_TYPE_LIST, FPTL_PARAM_LIST"
#endif

#include "FunctionTypes.hpp"

#include "Function0.hpp"
#include "Function1.hpp"
#include "Function2.hpp"
#include "Function3.hpp"
#include "Function4.hpp"
#include "Function5.hpp"
#include "Function6.hpp"
#include "Function7.hpp"
#include "Function8.hpp"
#include "Function9.hpp"
#include "Function10.hpp"

