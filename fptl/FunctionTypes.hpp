/*
 * Function Pointer Template Library V1.0
 * Copyright (c) 2013-2014 Christopher D. Granz
 *
 * www.christophergranz.com
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgement in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 * distribution.
 */

#pragma once

namespace fptl {

// dummy classes defined simply to get pointer sizes
class NoInheritA {};
class NoInheritB {};
class SingleInherit   : public NoInheritA {};
class MultipleInherit : public NoInheritA, public NoInheritB {};
class VirtualInherit  : virtual public NoInheritA {};
class Undefined;

// member function pointers of the various sizes
typedef void(NoInheritA::*MemberPtrNoInherit)();
typedef void(SingleInherit::*MemberPtrSingleInherit)();
typedef void(MultipleInherit::*MemberPtrMultipleInherit)();
typedef void(VirtualInherit::*MemberPtrVirtualInherit)();
typedef void(Undefined::*MemberPtrUndefined)();

inline static const int* GetPointerSizes()
{
	static const int pointerSizes[] =
	{
		(int) sizeof(void (*)()),
		(int) sizeof(MemberPtrNoInherit),
		(int) sizeof(MemberPtrSingleInherit),
		(int) sizeof(MemberPtrMultipleInherit),
		(int) sizeof(MemberPtrVirtualInherit),
		(int) sizeof(MemberPtrUndefined)
	};

	return pointerSizes;
}

} // namespace fptl

