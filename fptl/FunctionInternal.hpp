/*
 * NOTE: THIS FILE SHOULD NOT BE INCLUDED IN USER APPLICATIONS!
 * This is included inside the Function*.hpp headers after the preprocessor
 * #defines to generate the particular template class.
 */

/*
 * Function Pointer Template Library V1.0
 * Copyright (c) 2013-2014 Christopher D. Granz
 *
 * www.christophergranz.com
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgement in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 * distribution.
 */

template <FPTL_TEMPLATE_TYPENAME_ARGS> class FPTL_TEMPLATE_NAME
{
private:
	// this structure will vary in size with the type of class C
	template <typename C>
	struct MemberFuncData
	{
		C* obj;
		R (C::*func)(FPTL_PARAM_TYPE_LIST);
		inline MemberFuncData(C* object, R (C::*function)(FPTL_PARAM_TYPE_LIST)) : obj(object), func(function) { }
		inline R call(FPTL_PARAM_TYPE_LIST) const { return (obj->*func)(FPTL_PARAM_LIST); }
		inline bool equal(const MemberFuncData<C>* rhs) const { return (obj == rhs->obj && func == rhs->func); }
	};

	// the type of this pointer
	int type;

	// simple global function pointer
	R (*globalFunction)(FPTL_PARAM_TYPE_LIST);

	// reserved memory where we allocate a MemberFunction structure
	// we use sizeof(MemberFunction<Undefined>) because this should provide us with the maximum
	// size on any particular compiler
	unsigned char reserved[sizeof(MemberFuncData<Undefined>)];
	MemberFuncData<Undefined>* memberFuncData;

public:
	// constructors
	inline FPTL_TEMPLATE_NAME() { clear(); }
	inline FPTL_TEMPLATE_NAME(R (*function)(FPTL_PARAM_TYPE_LIST)) { assign(function); }
	template <typename C>
	inline FPTL_TEMPLATE_NAME(C* object, R (C::*function)(FPTL_PARAM_TYPE_LIST)) { assign(object, function); }
	inline FPTL_TEMPLATE_NAME(const FPTL_TEMPLATE_NAME& rhs) { operator=(rhs); }

	// assignment operator
	inline FPTL_TEMPLATE_NAME& operator=(const FPTL_TEMPLATE_NAME& rhs)
	{
		if (rhs.type < 0)
		{
			clear();
			return *this;
		}

		type = rhs.type;

		if (type == 0)
			globalFunction = rhs.globalFunction;
		else
		{
			for (unsigned int i = 0; i < sizeof(reserved); i++) // could use memcpy here
				reserved[i] = rhs.reserved[i];

			memberFuncData = (MemberFuncData<Undefined>*) &reserved[0];
		}

		return *this;
	}

	// assign nothing
	inline void clear() { type = -1; }

	// assign a global function
	inline void assign(R (*function)(FPTL_PARAM_TYPE_LIST))
	{
		if (function == ((R(*)(FPTL_PARAM_TYPE_LIST))0))
		{
			clear();
			return;
		}

		type = 0;
		globalFunction = function;
	}

	// assign a member function
	template <typename C>
	inline void assign(C* object, R (C::*function)(FPTL_PARAM_TYPE_LIST))
	{
		if (sizeof(function) == sizeof(MemberPtrNoInherit))
			type = 1;
		else if (sizeof(function) == sizeof(MemberPtrSingleInherit))
			type = 2;
		else if (sizeof(function) == sizeof(MemberPtrMultipleInherit))
			type = 3;
		else if (sizeof(function) == sizeof(MemberPtrVirtualInherit))
			type = 4;
		else
			type = 5;

//		memberFuncData = reinterpret_cast<MemberFuncData<Undefined>*>(new (&reserved) MemberFuncData<C>(object, function));

		// do it manually, rather than as above, which causes compilation issues on some platforms...
		MemberFuncData<C>* tmp = reinterpret_cast<MemberFuncData<C>*>(&reserved[0]);
		tmp->obj = object;
		tmp->func = function;
		memberFuncData = reinterpret_cast<MemberFuncData<Undefined>*>(&reserved[0]);
	}

	inline bool isSet() const { return (type >= 0); }

	// call the function based on its type
	inline R call(FPTL_PARAM_TYPE_LIST) const
	{
		switch (type)
		{
		case 0: return (*globalFunction)(FPTL_PARAM_LIST);
		case 1: return (reinterpret_cast<MemberFuncData<NoInheritA>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		case 2: return (reinterpret_cast<MemberFuncData<SingleInherit>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		case 3: return (reinterpret_cast<MemberFuncData<MultipleInherit>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		case 4: return (reinterpret_cast<MemberFuncData<VirtualInherit>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		default: return memberFuncData->call(FPTL_PARAM_LIST); // this will probably crash... lets use assert() here
		}
	}

	// operator() allows functor-style calling on this object
	inline R operator()(FPTL_PARAM_TYPE_LIST) const
	{
		switch (type)
		{
		case 0: return (*globalFunction)(FPTL_PARAM_LIST);
		case 1: return (reinterpret_cast<MemberFuncData<NoInheritA>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		case 2: return (reinterpret_cast<MemberFuncData<SingleInherit>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		case 3: return (reinterpret_cast<MemberFuncData<MultipleInherit>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		case 4: return (reinterpret_cast<MemberFuncData<VirtualInherit>*>(memberFuncData))->call(FPTL_PARAM_LIST);
		default: return memberFuncData->call(FPTL_PARAM_LIST); // this will probably crash... lets use assert() here
		}
	}

	// comparison operators
	inline bool operator==(const FPTL_TEMPLATE_NAME& rhs) const
	{
		if (type != rhs.type)
			return false;

		switch (type)
		{
		case 0: return (globalFunction == rhs.globalFunction);
		case 1: return (reinterpret_cast<MemberFuncData<NoInheritA>*>(memberFuncData))->equal(reinterpret_cast<MemberFuncData<NoInheritA>*>(rhs.memberFuncData));
		case 2: return (reinterpret_cast<MemberFuncData<SingleInherit>*>(memberFuncData))->equal(reinterpret_cast<MemberFuncData<SingleInherit>*>(rhs.memberFuncData));
		case 3: return (reinterpret_cast<MemberFuncData<MultipleInherit>*>(memberFuncData))->equal(reinterpret_cast<MemberFuncData<MultipleInherit>*>(rhs.memberFuncData));
		case 4: return (reinterpret_cast<MemberFuncData<VirtualInherit>*>(memberFuncData))->equal(reinterpret_cast<MemberFuncData<VirtualInherit>*>(rhs.memberFuncData));
		default: return false;
		}
	}

	inline bool operator!=(const FPTL_TEMPLATE_NAME& rhs) const { return !operator==(rhs); }
};

template <FPTL_TEMPLATE_TYPENAME_ARGS>
inline FPTL_TEMPLATE_NAME<FPTL_TEMPLATE_ARGS> Create(R (*function)(FPTL_PARAM_TYPE_LIST))
{
	return FPTL_TEMPLATE_NAME<FPTL_TEMPLATE_ARGS>(function);
}

template <typename C, FPTL_TEMPLATE_TYPENAME_ARGS>
inline FPTL_TEMPLATE_NAME<FPTL_TEMPLATE_ARGS> Create(C* object, R (C::*function)(FPTL_PARAM_TYPE_LIST))
{
	return FPTL_TEMPLATE_NAME<FPTL_TEMPLATE_ARGS>(object, function);
}

