The Function Pointer Template Library, or FPTL is a C++ header only library
which allows for the simple creation of pointers to global functions as well as
object member functions.  This library serves the same purpose as the
well-known FastDelegate library, but without all of the compiler-specific
definitions.  Testing shows that it generally performs similarly to the
FastDelegate library, except in the case of global function pointers, which are
generally faster.

BASIC USAGE
-----------

Function pointers may be defined for functions of any return type and zero to
ten arguments.  The template parameters should be provided with the first
parameter being the return type.

For example, to create a pointer which could point to the function,

float test(int x, char* str);

... you would write,

fptl::Function2<float, int, char*> myPointer;

... then to assign you may simply write,

myPointer = &test;

... and call with something like,

float result;
result = myPointer(10, "hello world");

To assign a member function you must provide its associated object which can be
done a few ways.  One way is with assign(), like so,

myPointer.assign(&myObj, &MyClass::testSomething);

See the included Example.cpp file for more example usage.

THEORY OF OPERATION
-------------------

The main issue with pointers to member functions is that they are not really
raw pointers under-the-hood, but are really composite pointers which are
implementation-specific and hidden from the user.  This means you can't simply
define a generic member function pointer and then have it point to arbitrary
member functions of different class.

Plan of attack:

1) Determine the different sizes of different function pointer types used by
the particular compiler.  Define different dummy classes which fall into the
different categories.
2) Define a buffer which is the largest size possible and allocate pointers
inside the buffer when an arbitrary member function pointer is assigned to our
special function pointer class.
3) Store a type code identifying the "type" of function pointer we have stored.
4) When calling the pointed-to-function, cast the generic pointer to a
dummy-class pointer which is of "type".  This causes the member function to be
called as intended.

Possible improvements:

-- Comparison operators other than == and != have not been overloaded, but
adding others would allow use in ordered containers.

BUILD INSTRUCTIONS
------------------

First of all, note that this library is just a set of header files.  You simply
include fptl/FPTL.hpp and off you go.  A small example program is included in
Example.cpp.  This is not exhaustive by any means, but it runs some quick tests,
showing performance of FPTL vs. FastDelegate and another similar library called
PlusCallback.  PlusCallback uses a similar technique as FPTL, but takes
advantage of virtual functions to decide how to call a member function.  This
seems to generally be slightly slower than the method used by FPTL.

Under *nix systems, simply type 'make' to build using the included Makefile.

Under Windows, a Visual Studio 2010 solution and project file is provided.