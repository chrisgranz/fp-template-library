PROGRAM=example
C_FILES=Example.cpp
O_FILES=Example.o

$(PROGRAM): $(O_FILES)
	g++ $(O_FILES) -o $(PROGRAM)

Example.o: Example.cpp
	g++ -O3 -c Example.cpp

clean:
	rm -f $(O_FILES) *~ $(PROGRAM)



