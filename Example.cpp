/*
 * Function Pointer Template Library V1.0
 * Copyright (c) 2013-2015 Christopher D. Granz
 *
 * www.christophergranz.com
 *
 * This software is provided 'as-is', without any express or implied
 * warranty. In no event will the authors be held liable for any damages
 * arising from the use of this software.
 *
 * Permission is granted to anyone to use this software for any purpose,
 * including commercial applications, and to alter it and redistribute it
 * freely, subject to the following restrictions:
 *
 * 1. The origin of this software must not be misrepresented; you must not
 * claim that you wrote the original software. If you use this software
 * in a product, an acknowledgment in the product documentation would be
 * appreciated but is not required.
 *
 * 2. Altered source versions must be plainly marked as such, and must not be
 * misrepresented as being the original software.
 *
 * 3. This notice may not be removed or altered from any source
 * distribution.
 */

// FPTL includes
#include "fptl/FPTL.hpp" // include all templates

// PlusCallback V1.7 library
#include "others/callback.hpp"

// FastDelegate library
#include "others/FastDelegate.h"
#include "others/FastDelegateBind.h"

#include <stdio.h>
#include <assert.h>
#include <stdlib.h>
#include <math.h>
#include <time.h>

#include <vector>

// number of iterations for each call test
#define TEST_ITERATIONS    500000

// our function prototype for testing is:
//
//   double func(int a, double b, double c)
//

// this is our type (FPTL)
typedef fptl::Function3<double, int, double, double> MyFPTLType;

// fastdelegate -- return type is last template parameter
typedef fastdelegate::FastDelegate3<int, double, double, double> MyFastDelegateType;

// this is from PlusCallback V1.7
typedef cb::Callback3<double, int, double, double> MyPlusCallbackType;

// lets create a few classes for testing purposes
class MyClass_A
{
private:
	double x;
	double y;

public:
	MyClass_A(double a, double b) : x(a), y(b) {}

	double computeSomething(int a, double b, double c)
	{
		double result = sin(x+y);

		return result;
	}
};

class MyClass_B
{
private:
	int q;

public:
	MyClass_B(int a) : q(a) {}

	virtual double computeSomethingElse(int a, double b, double c)
	{
		return cos(b*c)+a+b;
	}
};

class MyClass_C : public MyClass_A, public MyClass_B
{
private:
	int z;

public:
	MyClass_C(int a) : MyClass_A(1.0, 2.0), MyClass_B(3), z(a) {}

	virtual double computeSomethingElse(int a, double b, double c)
	{
		return cos(b*c) + sin((double)(a+z));
	}
};

double GlobalComputeSomething(int a, double b, double c)
{
	return cos(b*c);
}

// some test objects
MyClass_A myClassA(42.0, 2.7);
MyClass_B myClassB(4);
MyClass_C myClassC(10);

// test functions for each library
static double TestFastDelegate(MyFastDelegateType& myFastDelegate, int count)
{
	double result = 0.0;

	for (int k = 0; k < count; k++)
	{
		for (int i = 0; i < TEST_ITERATIONS; i++)
			result += myFastDelegate(1, 2.0, 3.0);
	}

	return result;
}

static double TestPlusCallback(MyPlusCallbackType& myPlusCallback, int count)
{
	double result = 0.0;

	for (int k = 0; k < count; k++)
	{
		for (int i = 0; i < TEST_ITERATIONS; i++)
			result += myPlusCallback(1, 2.0, 3.0);
	}

	return result;
}

static double TestFPTL(MyFPTLType& myFunc, int count)
{
	double result = 0.0;

	for (int k = 0; k < count; k++)
	{
		for (int i = 0; i < TEST_ITERATIONS; i++)
			result += myFunc(1, 2.0, 3.0);
	}

	return result;
}

// Runs the speed tests
static void RunSpeedTests(MyFastDelegateType& myFastDelegate, MyPlusCallbackType& myPlusCallback, MyFPTLType& myFunc, int count)
{
	clock_t timeBegin;
	clock_t timeEnd;
	double computationResult;
	double totalTimeSpent;
	double perCallTimeSpent;

	//printf("               computation result      total time (s)    per call (s)\n");
	printf("               computation result      total time (s)\n");

	// -----------------------------------------------------------------------
	// Test FastDelegate
	// -----------------------------------------------------------------------
	timeBegin = clock();

	computationResult = TestFastDelegate(myFastDelegate, 100);

	timeEnd = clock();
	totalTimeSpent = (double)(timeEnd - timeBegin) / CLOCKS_PER_SEC;
	perCallTimeSpent = totalTimeSpent / TEST_ITERATIONS;

	//printf("fastdelegate   %16f       %9f     %16f\n",
	//	computationResult, totalTimeSpent, perCallTimeSpent);
	printf("fastdelegate    %16f        %9f\n",
		computationResult, totalTimeSpent);

	// -----------------------------------------------------------------------
	// Test PlusCallback
	// -----------------------------------------------------------------------
	timeBegin = clock();

	computationResult = TestPlusCallback(myPlusCallback, 100);

	timeEnd = clock();
	totalTimeSpent = (double)(timeEnd - timeBegin) / CLOCKS_PER_SEC;
	perCallTimeSpent = totalTimeSpent / TEST_ITERATIONS;

	//printf("pluscallback   %16f       %9f     %16f\n",
	//	computationResult, totalTimeSpent, perCallTimeSpent);
	printf("pluscallback    %16f        %9f\n",
		computationResult, totalTimeSpent);

	// -----------------------------------------------------------------------
	// Test FPTL
	// -----------------------------------------------------------------------
	timeBegin = clock();

	computationResult = TestFPTL(myFunc, 100);

	timeEnd = clock();
	totalTimeSpent = (double)(timeEnd - timeBegin) / CLOCKS_PER_SEC;
	perCallTimeSpent = totalTimeSpent / TEST_ITERATIONS;

	//printf("fptl           %16f       %9f     %16f\n",
	//	computationResult, totalTimeSpent, perCallTimeSpent);
	printf("fptl            %16f        %9f\n",
		computationResult, totalTimeSpent);
}

int main(int argc, char *argv[])
{
	// check and print out function pointer sizes on this system
	const int* pointerSizes = fptl::GetPointerSizes();

	printf("----------------- function pointer sizes ---------------------\n");
	printf("     Global Function: %d bytes\n", pointerSizes[0]);
	printf("      No Inheritance: %d bytes\n", pointerSizes[1]);
	printf("  Single Inheritance: %d bytes\n", pointerSizes[2]);
	printf("Multiple Inheritance: %d bytes\n", pointerSizes[3]);
	printf(" Virtual Inheritance: %d bytes\n", pointerSizes[4]);
	printf("           Undefined: %d bytes\n", pointerSizes[5]);
	printf("--------------------------------------------------------------\n");

	printf("\nRunning tests with %ld iterations...\n", (long)TEST_ITERATIONS);
	printf("NOTE: Computation results are meaningless\n"
		"(although they should be the same for each library)\n");

	MyFastDelegateType myFastDelegate;
	MyPlusCallbackType myPlusCallback;
	MyFPTLType myFunc;

	printf("\n----------------- global function speed test -----------------\n");

	myFastDelegate = GlobalComputeSomething;
	myPlusCallback = GlobalComputeSomething;
	myFunc = GlobalComputeSomething;

	RunSpeedTests(myFastDelegate, myPlusCallback, myFunc, 500);

	printf("--------------------------------------------------------------\n");

	printf("\n----------------- no-inheritance speed test ------------------\n");

	myFastDelegate = fastdelegate::MakeDelegate(&myClassA, &MyClass_A::computeSomething);
	myPlusCallback = cb::Make3(&myClassA, &MyClass_A::computeSomething);
	myFunc = fptl::Create(&myClassA, &MyClass_A::computeSomething);

	RunSpeedTests(myFastDelegate, myPlusCallback, myFunc, 50);

	printf("--------------------------------------------------------------\n");

	printf("\n------------- multiple-inheritance speed test ----------------\n");

	myFastDelegate = fastdelegate::MakeDelegate(&myClassC, &MyClass_C::computeSomethingElse);
	myPlusCallback = cb::Make3(&myClassC, &MyClass_C::computeSomethingElse);
	myFunc = fptl::Create(&myClassC, &MyClass_C::computeSomethingElse);

	RunSpeedTests(myFastDelegate, myPlusCallback, myFunc, 50);

	printf("--------------------------------------------------------------\n");

	printf("Press any key to exit...");
	getchar();
	return 0;
}

